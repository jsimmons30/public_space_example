﻿using System;

namespace ShinApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Shin's D&D Dice!");
            Console.WriteLine("This will show 1D4, 1D6, and 1D20 dice rolls");

            var x = D4Class.RNG4Numbers();
            Console.WriteLine(x.ToString());

            var y = D6Class.RNG6Numbers();
            Console.WriteLine(y.ToString());

            var z = D20Class.RNG20Numbers();
            Console.WriteLine(z.ToString());

           // Console.ReadLine();
        }
    }
}
